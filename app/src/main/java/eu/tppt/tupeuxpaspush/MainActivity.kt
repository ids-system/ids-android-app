package eu.tppt.tupeuxpaspush

import android.media.RingtoneManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.pusher.pushnotifications.PushNotifications
import com.google.firebase.messaging.RemoteMessage
import com.pusher.pushnotifications.PushNotificationReceivedListener

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (getIntent().getStringExtra("isMyPushNotification") != null) {
            // This means that the onCreate is the result of a notification being opened
            Log.i("MyActivity", "Notification incoming!")
            try {
                val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
                val r = RingtoneManager.getRingtone(applicationContext, notification)
                r.play()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        setContentView(R.layout.activity_main)
        PushNotifications.start(getApplicationContext(),  getString(R.string.pusherid))
        PushNotifications.subscribe("alert")
    }

    override fun onResume() {
        super.onResume()
        PushNotifications.setOnMessageReceivedListenerForVisibleActivity(this, object : PushNotificationReceivedListener {
            override fun onMessageReceived(remoteMessage: RemoteMessage) {

                val messagePayload = remoteMessage.data["reason"]
                if (messagePayload == null) {
                    // Message payload was not set for this notification
                    Log.i("MyActivity", "Payload was missing")
                } else {
                    Log.i("MyActivity", messagePayload)
                    if(messagePayload=="ALARM"){
                        try {
                            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
                            val r = RingtoneManager.getRingtone(applicationContext, notification)
                            r.play()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {

                    }



                    // Now update the UI based on your message payload!
                }
            }
        })
    }
}
