package eu.tppt.tupeuxpaspush
import android.util.Log
import com.google.firebase.messaging.RemoteMessage
import com.pusher.pushnotifications.fcm.MessagingService
import android.content.Intent

class NotificationsMessagingService : MessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.i("NotificationsService", "Got a remote message 🎉")
        val resultIntent = Intent(applicationContext, MainActivity::class.java)
        resultIntent.putExtra("isMyPushNotification","true")
        startActivity(resultIntent)
    }
}